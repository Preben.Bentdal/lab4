package cellular;

import java.util.Random;

import datastructure.CellGrid;
import datastructure.IGrid;

/**
 * 
 * A CellAutomata that implements Conways game of life.
 * 
 * @see CellAutomaton
 * 
 *      Every cell has two states: Alive or Dead. Each step the state of each
 *      cell is decided from its neighbors (diagonal, horizontal and lateral).
 *      If the cell has less than two alive Neighbors or more than three
 *      neighbors the cell dies. If a dead cell has exactly three neighbors it
 *      will become alive.
 * 
 * @author eivind
 * @author Martin Vatshelle - martin.vatshelle@uib.no
 * @author Sondre Bolland - sondre.bolland@uib.no
 */
public class GameOfLife implements CellAutomaton {

	/**
	 * The grid of cells
	 */
	IGrid currentGeneration;
	int stepc;

	/**
	 * 
	 * Construct a Game Of Life Cell Automaton that holds cells in a grid of the
	 * provided size
	 * 
	 * @param height The height of the grid of cells
	 * @param width  The width of the grid of cells
	 */
	public GameOfLife(int rows, int columns) {
		currentGeneration = new CellGrid(rows, columns, CellState.DEAD);
		initializeCells();
	}

	@Override
	public void initializeCells() {
		Random random = new Random();
		for (int row = 0; row < currentGeneration.numRows(); row++) {
			for (int col = 0; col < currentGeneration.numColumns(); col++) {
				if (random.nextBoolean()) {
					currentGeneration.set(row, col, CellState.ALIVE);
				} else {
					currentGeneration.set(row, col, CellState.DEAD);
				}
			}
		}
	}

	@Override
	public int numberOfRows() {
	
		return currentGeneration.numRows();
	}

	@Override
	public int numberOfColumns() {
		return currentGeneration.numColumns();
	}

	@Override
	public CellState getCellState(int row, int col) {
		
		return currentGeneration.get(row, col);
	}

	@Override
	public void step() {
		stepc++;
		System.out.println(stepc);
		
		IGrid nextGeneration = currentGeneration.copy();
		for (int i = 0; i<numberOfRows(); i++){
            for (int m = 0; m<numberOfColumns(); m++){
                nextGeneration.set(i, m, getNextCell(i, m));
            }    
        }
		this.currentGeneration = nextGeneration;
		
	}
    
	
	@Override
	public CellState getNextCell(int row, int col) {
		CellState stateofcell = getCellState(row, col);
		int neighboursAlive = countNeighbors(row, col, CellState.ALIVE);
		if ((stateofcell == CellState.DEAD) && (neighboursAlive == 3)){
			stateofcell = CellState.ALIVE;

		}
		else if (stateofcell == CellState.ALIVE){
			if ((neighboursAlive<2) || (neighboursAlive>3)){
				stateofcell = CellState.DEAD;
			}else{
				stateofcell = CellState.ALIVE;
		    }}
		return stateofcell;
		}
		
		
		
	

	/**
	 * Calculates the number of neighbors having a given CellState of a cell on
	 * position (row, col) on the board
	 * 
	 * Note that a cell has 8 neighbors in total, of which any number between 0 and
	 * 8 can be the given CellState. The exception are cells along the boarders of
	 * the board: these cells have anywhere between 3 neighbors (in the case of a
	 * corner-cell) and 5 neighbors in total.
	 * 
	 * @param x     the x-position of the cell
	 * @param y     the y-position of the cell
	 * @param state the Cellstate we want to count occurences of.
	 * @return the number of neighbors with given state
	 */
	public int countNeighbors(int row, int col, CellState state) {
		int neighbors = 0;
		for (int i = (row-1); i<= (row+1); i++){
			if(i > numberOfColumns()){return neighbors;}
			
			
			try {
				getCellState(i,0);
			}
			catch(ArrayIndexOutOfBoundsException exception) {
				if (i==numberOfRows()){
					break;}
				else{
				i++;}}

			for (int m = (col-1); m<= (col+1); m++){
				if (m > numberOfRows()) {return neighbors;}
				
				try {
					getCellState(i,m);
				}
				catch(ArrayIndexOutOfBoundsException exception) {
					if (m==numberOfRows()){
						break;

					} 
					else{
						m++;}

				}
				if ((i == row) && (m == col)){
				}
				else if  (getCellState(i,m) == state){
					neighbors++;
				}

			}}
		return neighbors;
	}

	@Override
	public IGrid getGrid() {
		return currentGeneration;
	}

	public static void main(String[] args){
		CellAutomaton ca= new GameOfLife(100, 100);
		ca.step();
		System.out.println(CellState.DEAD == CellState.DEAD);
		
		
		
		

	}
}
