package datastructure;
import java.awt.*;    
import javax.swing.*;   
import cellular.CellAutomaton;
import cellular.CellState;

public class CellGrid implements IGrid {

    int rows;
    int cols;
    CellState[][] cellStates;
    CellAutomaton ca;
    
    

    public CellGrid(int rows, int columns, CellState initialState) {
		this.rows = rows;
        this.cols = columns;
        this.cellStates = new CellState[rows][columns];
        for (int i = 0; i<rows; i++){
            for (int m = 0; m<cols; m++){
                this.cellStates[i][m] = initialState;
            }    
        }
        

        
	}

    @Override
    public int numRows() {
        return this.rows;
    }

    @Override
    public int numColumns() {
        return this.cols;
    }

    @Override
    public void set(int row, int column, CellState element) {
        this.cellStates[row][column] = element;
        
    }

    @Override
    public CellState get(int row, int column) {
        CellState x = this.cellStates[row][column];
        return x;
    }

    @Override
    public IGrid copy() {
        CellGrid copi= new CellGrid(this.rows, this.cols, null);
        for (int i = 0; i<this.rows; i++){
            for (int m = 0; m<this.cols; m++){
                copi.set(i,m,get(i,m));
            }}
        return copi;
    }
    public static void main(String[] args){
        CellGrid ca = new CellGrid(1, 1, CellState.DEAD);
        System.out.println(ca.numRows());
        System.out.println(ca.numColumns());

    }
}
